const listFilter = (wantedValue, sourceList) => {

    let numAppearances = 0;

    // for (value of sourceList) {numAppearances = (value == wantedValue ? numAppearances += 1 : numAppearances)};

    // (a && b) retorna false se a=false, retorna b se a=true. b faz a adição unitária ao numAppearances
    for (value of sourceList) { value === wantedValue && (numAppearances += 1) }

    return numAppearances
}

module.exports = { listFilter }