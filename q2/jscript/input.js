const prompt = require('prompt');

const inputWantedValue = async () => {
    console.log('\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n');
    console.log('Insira o valor procurado: ')

    const inputSearch = await prompt.get(['Valor']);
    searchedValue = parseInt(inputSearch.Valor)

    return searchedValue
}


const inputSourceList = async () => {

    let sourceList = [];

    console.log('\nInsira os valores que compõem a lista de busca.');
    console.log('*Caso queira parar, insira um valor negativo.\n');

    for (let i = 0; i < 20; i++) {

        const inputValue = await prompt.get([`Valor ${i + 1}`]);
        const insertedValue = parseInt(inputValue[`Valor ${i + 1}`])

        if (insertedValue < 0) {
            break
        } else {
            sourceList.push(insertedValue);
        }
    }

    return sourceList
}

module.exports = { inputWantedValue, inputSourceList }