const { inputSourceList, inputWantedValue } = require('./input')
const { listFilter } = require('./processing')
const { output } = require('./output')


const main = async () => {

    const wantedValue = await inputWantedValue()
    const sourceList = await inputSourceList()
    const numAppearances = listFilter(wantedValue, sourceList)

    output(wantedValue, numAppearances)

}


main()