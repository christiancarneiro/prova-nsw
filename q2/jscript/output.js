const output = (wantedValue, numAppearances) => console.log(`O número ${wantedValue} foi encontrado ${numAppearances}x!"`)

module.exports = { output }