from input import input_source_list, input_wanted_value
from processing import count_filter
from output import output


def main():

    wanted_value = input_wanted_value()
    source_list = input_source_list()
    n_appearances = count_filter(wanted_value, source_list)

    output(wanted_value, n_appearances)


main()
