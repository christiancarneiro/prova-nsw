def input_wanted_value():

    print('\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n')
    print('DETETIVE DE VALORES')
    wanted_value = int(input('Insira o valor inteiro a ser procurado: '))

    return wanted_value


def input_source_list():

    source_list = []
    print('\nInsira os valores que compõem a lista de busca.')
    print('*Caso queira parar, insira um valor negativo.\n')

    for i in range(20):
        entered_value = int(input(f'Insira o {i+1}º valor inteiro: '))

        if entered_value > 0:
            source_list.append(entered_value)
        else:
            break

    return source_list
