from input import input_data
from processing import validate, make_displacement
from output import output


def main():

    num_displacements, list_entries = input_data()
    flag, message = validate(num_displacements, list_entries)

    if flag:
        list_result = make_displacement(num_displacements, list_entries)
        output(list_result)
    else:
        print(message)


main()
