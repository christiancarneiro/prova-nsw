def validate(_num_displacements,  _list_entries):

    num_entries = len(_list_entries)

    if _num_displacements < 0 or _num_displacements > num_entries:
        return False, "Erro: Número de deslocamentos incompatível"

    for entry in _list_entries:
        if entry > 10**5:
            return False, "Erro: Valor de entrada > 100000"

    return True, "Valores válidos"


def make_displacement(_num_displacements, _list_entries):

    list_initial = _list_entries[:_num_displacements]
    list_final = _list_entries[_num_displacements:]

    list_result = list_final + list_initial

    return list_result
