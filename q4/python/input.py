def input_data():

    print('\n---------------------------------')
    print('\nDESLOCADOR DE VETORES\n')

    print('\nInsira os valores desejados: ')
    num_displacements = int(
        input('Número de deslocamentos à esquerda: '))
    num_entries = int(
        input('Número de entradas da lista: '))

    list_entries = []

    for i in range(num_entries):
        list_entries.append(int(input(f'Valor {i+1}: ')))

    print('\n---------------')
    print(f'Lista inserida: {list_entries}')
    print(f'Número de deslocamentos à esquerda: {num_displacements}')
    print('---------------')

    return num_displacements, list_entries
