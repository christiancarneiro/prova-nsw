const prompt = require('prompt');


const inputData = async () => {

    console.log('\n---------------------------------')
    console.log('\nDESLOCADOR DE VETORES\n')

    console.log('Insira os valores desejados')

    console.log('Número de deslocamentos à esquerda: ')
    const inputNumDisplacements = await prompt.get(['deslocamentos']);
    const numDisplacements = parseInt(inputNumDisplacements.deslocamentos)

    console.log('\nNúmero de entradas da lista: ')
    const inputNumEntries = await prompt.get(['entradas']);
    const numEntries = parseInt(inputNumEntries.entradas)

    let listEntries = []

    for (let i = 0; i < numEntries; i++) {
        const inputEntry = await prompt.get([`entrada ${i + 1}`]);
        const entry = parseInt(inputEntry[`entrada ${i + 1}`])
        listEntries.push(entry)
    }

    return { numDisplacements, listEntries }

}

module.exports = { inputData }