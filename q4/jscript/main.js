const { inputData } = require('./input')
const { validate, makeDisplacement } = require('./processing')
const { output } = require('./output')


const main = async () => {

    const { numDisplacements, listEntries } = await inputData()
    const flag = validate(numDisplacements, listEntries)

    // mudar isso
    if (flag.test) {
        const listResult = makeDisplacement(numDisplacements, listEntries)
        output(listResult)
    }
    else {
        console.log(flag.msg)
    }
}

main()