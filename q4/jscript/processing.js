const validate = (numDisplacements, listEntries) => {

    const numEntries = listEntries.length

    if (numDisplacements < 0 || numDisplacements > numEntries) {
        return { 'test': false, 'msg': "Erro: Número de deslocamentos incompatível" }
    }

    for (entry of listEntries) {
        if (entry > 10 ** 5) { return { 'test': false, 'msg': "Erro: Valor de entrada > 100000" } }
    }

    return { 'test': true, 'msg': "Valores válidos" }
}


const makeDisplacement = (numDisplacements, listEntries) => {

    const listInitial = listEntries.splice(0, numDisplacements)
    const listResult = listEntries.concat(listInitial)

    return listResult
}


module.exports = { validate, makeDisplacement }