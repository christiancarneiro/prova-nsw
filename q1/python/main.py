from input import ask_parameters
from processing import imc_status, imc_value
from output import output


def main():

    height, weight = ask_parameters()
    imc = imc_value(height, weight)
    status = imc_status(imc)

    output(imc, status)


main()
