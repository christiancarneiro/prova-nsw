def ask_parameters():

    print('\n~~~~~~~~~~~~~~~~~~~~')
    print('\n< CALCULADORA DE IMC >')
    print('\nInsira os dados solicitados:\n')
    height = float(input('Altura (m): '))
    weight = float(input('Peso (kg): '))

    return height, weight
