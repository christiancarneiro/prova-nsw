def imc_value(_height, _weight):

    imc = round(_weight/(_height**2), 2)
    return imc


def imc_status(_imc):

    if _imc >= 40:
        status = "Obesidade grau III"
    elif _imc >= 35:
        status = "Obesidade grau II"
    elif _imc >= 30:
        status = "Obesidade grau I"
    elif _imc >= 25:
        status = "sobrepeso"
    elif _imc >= 18.5:
        status = "Peso normal"
    elif _imc >= 17:
        status = "Abaixo do peso"
    else:
        status = "Muito abaixo do peso"

    return status
