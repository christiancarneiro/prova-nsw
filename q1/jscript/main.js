const { inputData } = require('./input')
const { imcValue, imcStatus } = require('./processing');
const { output } = require('./output')


const main = async () => {

    const { height, weight } = await inputData();
    const userImc = imcValue(height, weight);
    const userStatus = imcStatus(userImc)

    output(userImc, userStatus)
}


main()
