const imcValue = (height, weight) => +((weight / (height ** 2)).toFixed(2))


const imcStatus = (imc) => {


    if (imc >= 40) {
        return "Obesidade grau III";
    } else if (imc >= 35) {
        return "Obesidade grau II";
    } else if (imc >= 30) {
        return "Obesidade grau I";
    } else if (imc >= 25) {
        return "Sobrepeso";
    } else if (imc >= 18.5) {
        return "Peso normal";
    } else if (imc >= 17) {
        return "Abaixo do peso";
    } else {
        return "Muito abaixo do peso.";
    }

}

module.exports = { imcValue, imcStatus }