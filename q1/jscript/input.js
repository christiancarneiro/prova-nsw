const prompt = require('prompt');

async function inputData() {

    console.log('\n~~~~~~~~~~~~~~~~~~~~');
    console.log('\n< CALCULADORA DE IMC >');
    console.log('\nInsira os dados solicitados:\n');


    prompt.start()
    let inputMeasures = await prompt.get(['altura (m)', 'peso (kg)']);

    height = parseFloat(inputMeasures['altura (m)'])
    weight = parseFloat(inputMeasures['peso (kg)'])

    return { height, weight }
}

module.exports = { inputData }

