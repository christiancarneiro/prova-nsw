const output = (imc, imcStatus) => {
    console.log('\n----------------');
    console.log(`*seu IMC é: ${imc},\n*seu status é: ${imcStatus}.`);
    console.log('----------------\n');
    console.log('~~~~~~~~~~~~~~~~~~~~\n');
}

module.exports = { output }