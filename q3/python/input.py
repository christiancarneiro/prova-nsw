def input_cattle():

    print('\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n')

    n_cattle = int(
        input('Insira o número de bois que compõem o seu rebanho: '))
    cattle = {}

    for i in range(n_cattle):

        print(f'\nInsira as seguintes informações acerca do {i+1}º boi:')

        id = int(input('Número de identificação: '))
        name = input(f'Nome: ')
        weight = round(float(input('Peso (kg): ')), 2)

        cattle[id] = {
            'name': name,
            'weight': round(weight, 2)
        }

    print('Bois registrados:')
    for id in cattle.keys():
        print(
            f'id: {id} | nome: {cattle[id]["name"]} | peso {cattle[id]["weight"]} kg')

    return cattle
