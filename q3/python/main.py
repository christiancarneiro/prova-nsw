from input import input_cattle
from processing import filter_cattle
from output import output


def main():

    cattle = input_cattle()
    thinnest, fattest = filter_cattle(cattle)

    output(thinnest, fattest)


main()
