def output(_thinnest, _fattest):

    # os valores salvos são tuplas (key, value), onde key é a id do boi, value é um dict contendo as características nome e peso.

    print('\n----------------------------------')
    print(
        f"Gordo: id: {_fattest[0]} | nome: {_fattest[1]['name']} | peso: {_fattest[1]['weight']} kg")
    print(
        f"Magro: id: {_thinnest[0]} | nome: {_thinnest[1]['name']} | peso: {_thinnest[1]['weight']} kg")
    print('----------------------------------')
    print('\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
