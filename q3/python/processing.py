def filter_cattle(_cattle):

    sorted_cattle = sorted(_cattle.items(), key=lambda k: k[1]['weight'])

    # os valores estão dispostos em ordem de peso, do menor para o maior. índice do menor: 0, índice do maior: -1.

    thinnest = sorted_cattle[0]
    fattest = sorted_cattle[-1]

    return thinnest, fattest
