filterCattle = (cattle) => {

    cattle.sort((itemA, itemB) => { return itemA.weight - itemB.weight })

    result = {
        'thinnest': cattle[0],
        'fattest': cattle[cattle.length - 1]
    }

    return result
}

module.exports = { filterCattle }