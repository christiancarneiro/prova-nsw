const { inputCattle } = require('./input')
const { filterCattle } = require('./processing')
const { output } = require('./output')


const main = async () => {

    let cattle = await inputCattle()
    let result = filterCattle(cattle)

    output(result)
}


main()