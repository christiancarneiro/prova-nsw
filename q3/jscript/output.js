const output = (result) => {

    console.log('\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n')
    console.log("Resultado:")
    console.log(`Gordo: id: ${result.fattest.id} | nome: ${result.fattest.name} | peso: ${result.fattest.weight} kg`)
    console.log(`Magro: id: ${result.thinnest.id} | nome: ${result.thinnest.name} | peso: ${result.thinnest.weight} kg`)
    console.log('\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n')
}


module.exports = { output }