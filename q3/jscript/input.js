const prompt = require('prompt');


inputCattle = async () => {

    console.log('\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n')
    console.log('REGISTRADOR DE BOIS\n')
    console.log('Insira o número de bois que compõem o seu rebanho: ')

    const resultNum = await prompt.get('número');
    numCattle = parseInt(resultNum['número'])

    let cattle = []

    for (let i = 0; i < numCattle; i++) {

        console.log(`\nInsira as seguintes informações acerca do ${i + 1}º boi:`)

        const inputInfo = await prompt.get(['id', 'name', 'weight']);

        inputInfo.weight = +(parseFloat(inputInfo.weight).toFixed(2))
        cattle.push(inputInfo)
    }

    console.log('\nBois registrados: ')
    for (ox of cattle) { console.log(`id: ${ox.id} | nome: ${ox.name} | peso ${ox.weight} kg`) }

    return cattle
}


module.exports = { inputCattle }